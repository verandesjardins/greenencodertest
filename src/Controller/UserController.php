<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends AbstractController
{
    #[Route('/users', name: 'api_user_videos')]
    public function getUserVideos($userId, EntityManagerInterface $entityManager)
    {
        $user = $entityManager->getRepository(User::class)->find($userId);

        if (!$user) {
            throw $this->createNotFoundException('User not found');
        }

        $videos = $user->getVideos();

        $videosList = [];
        foreach ($videos as $video) {
            $videosList[] = [
                'id' => $video->getId(),
                'name' => $video->getName()
            ];
        }

        return new JsonResponse($videosList);
    }

    public function getReport($userId, EntityManagerInterface $entityManager)
    {
        $jsonVideos = $this->getUserVideos($userId, $entityManager);
        if (!$jsonVideos->getContent()) {
            throw $this->createNotFoundException('No videos found');
        }

        $videos = json_decode($jsonVideos->getContent());

        $folders = [];

        $report = [];
        foreach ($videos as $video) {
            $videoObject = $entityManager->getRepository(Video::class)->find($video->id);
            $viewsNumber = $videoObject->getViews();
            $videoSize = $videoObject->getSize();
            $encodedVideoSize = $videoObject->getEncodedSize();
            $videoTotalSize = $viewsNumber * $videoSize;
            $co2Emission = $viewsNumber * $videoSize / (1024 * 1024) * 0.05;
            $encodedVideoTotalSize = $viewsNumber * $encodedVideoSize;
            $encodedVideoCo2Emission = $viewsNumber * $encodedVideoSize / (1024 * 1024) * 0.05;
            $carbonSavings = $co2Emission - $encodedVideoCo2Emission;
            $sizeReduction = $carbonSavings / $co2Emission * 100;
            $report[] = [
                'folders' => $folders,
                'video_name' => $videoObject->getName(),
                'video_quality' => $videoObject->getQuality()->getLabel(),
                'video_views' => $viewsNumber,
                'video_size' => $videoSize,
                'video_total_size' => $videoTotalSize,
                'co2_emission' => $co2Emission,
                'encoded_video_size' => $encodedVideoSize,
                'encoded_video_total_size' => $encodedVideoTotalSize,
                'encoded_video_co2_emission' => $encodedVideoCo2Emission,
                'carbon_savings' => $carbonSavings,
                'size_reduction' => $sizeReduction,
            ];
        }

        return new JsonResponse($report);
    }
}