<?php

namespace App\Entity;

use App\Repository\EncodedVideosRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EncodedVideosRepository::class)]
class EncodedVideos
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'encodedVideo')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Video $video;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $views = null;

    public function getId(): ?int
    {
        return $this->id;
    }








    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(?int $views): static
    {
        $this->views = $views;

        return $this;
    }
}
