<?php

namespace App\Entity;

use App\Repository\VideoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VideoRepository::class)]
class Video
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $duration = null;

    #[ORM\Column(type: Types::BIGINT)]
    private ?int $size = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?VideoQuality $quality = null;

    #[ORM\ManyToOne(inversedBy: 'videos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Folder $folder = null;

    #[ORM\ManyToOne(inversedBy: 'videos')]
    private ?User $user = null;

    #[ORM\OneToOne(mappedBy: 'video', cascade: ['persist', 'remove'])]
    private ?EncodedVideo $encodedVideo = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): static
    {
        $this->duration = $duration;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): static
    {
        $this->size = $size;

        return $this;
    }

    public function getQuality(): ?VideoQuality
    {
        return $this->quality;
    }

    public function setQuality(?VideoQuality $quality): static
    {
        $this->quality = $quality;

        return $this;
    }

    public function getFolder(): ?Folder
    {
        return $this->folder;
    }

    public function setFolder(?Folder $folder): static
    {
        $this->folder = $folder;

        return $this;
    }
    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getEncodedVideo(): ?EncodedVideo
    {
        return $this->encodedVideo;
    }

    public function setEncodedVideo(EncodedVideo $encodedVideo): static
    {
        // set the owning side of the relation if necessary
        if ($encodedVideo->getVideo() !== $this) {
            $encodedVideo->setVideo($this);
        }

        $this->encodedVideo = $encodedVideo;

        return $this;
    }

    public function getViews()
    {
        $encodedVideo = $this->getEncodedVideo();
        if ($encodedVideo) {
            return $encodedVideo->getViews();
        }
        else {
            return 0;
        }
    }

    public function getEncodedSize()
    {
        $encodedVideo = $this->getEncodedVideo();
        if ($encodedVideo) {
            return $encodedVideo->getSize();
        }
        else {
            return 0;
        }
    }
}
