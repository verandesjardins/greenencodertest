<?php

namespace App\DataFixtures;

use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class VideoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $videoQuality = $this->getReference('video_quality');
        $wifiFolder = $this->getReference('nouveau_code_wifi');
        $tf1Folder = $this->getReference('tf1');
        $samsungFolder = $this->getReference('samsung');
        $youtubeFolder = $this->getReference('youtube');
        $user1 = $this->getReference('user1');
        $user2 = $this->getReference('user2');

        $video1 = new Video();
        $video1->setName('FACEBOOK_BOUYGUES_ULTYM_15s_VF_16-9_WEB');
        $video1->setDuration(15);
        $video1->setSize(28501260);
        $video1->setQuality($videoQuality);
        $video1->setFolder($wifiFolder);
        $video1->setUser($user1);
        $manager->persist($video1);

        $video2 = new Video();
        $video2->setName('TF1_BOUYGUES_ULTYM_30s_VF_16-9_WEB');
        $video2->setDuration(30);
        $video2->setSize(673030420);
        $video2->setQuality($videoQuality);
        $video2->setFolder($tf1Folder);
        $video2->setUser($user2);
        $manager->persist($video2);

        $video3 = new Video();
        $video3->setName('SAMSUNG_BOUYGUES_ULTYM_30s_VF_16-9_WEB');
        $video3->setDuration(30);
        $video3->setSize(57221234);
        $video3->setQuality($videoQuality);
        $video3->setFolder($samsungFolder);
        $video3->setUser($user1);
        $manager->persist($video3);

        $video4 = new Video();
        $video4->setName('YOUTUBE_BOUYGUES_ULTYM_15s_VF_16-9_WEB');
        $video4->setDuration(15);
        $video4->setSize(321030996);
        $video4->setQuality($videoQuality);
        $video4->setFolder($youtubeFolder);
        $video4->setUser($user2);
        $manager->persist($video4);

        $video5 = new Video();
        $video5->setName('TF1_BOUYGUES_ULTYM_15s_VF_16-9_WEB');
        $video5->setDuration(15);
        $video5->setSize(321030996);
        $video5->setQuality($videoQuality);
        $video5->setFolder($tf1Folder);
        $video5->setUser($user1);
        $manager->persist($video5);

        $video6 = new Video();
        $video6->setName('TF1_BOUYGUES_ULTYM_30s_VF_16-9_WEB_NO_MS');
        $video6->setDuration(30);
        $video6->setSize(656772468);
        $video6->setQuality($videoQuality);
        $video6->setFolder($tf1Folder);
        $video6->setUser($user2);
        $manager->persist($video6);

        $manager->flush();

        // These references will be used inside encoded video fixture
        $this->addReference('video1', $video1);
        $this->addReference('video2', $video3);
        $this->addReference('video3', $video5);
    }

    public function getDependencies(): array
    {
        return [
            VideoQualityFixtures::class,
            FolderFixtures::class,
            UserFixtures::class,
        ];
    }
}