<?php

namespace App\DataFixtures;

use App\Entity\Folder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FolderFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        echo "load FolderFixtures";
        $folder1 = new Folder();
        $folder1->setName('Meta');
        $manager->persist($folder1);

        $folder2 = new Folder();
        $folder2->setName('DIGITAL-selected');
        $folder2->setParent($folder1);
        $manager->persist($folder2);

        $folder3 = new Folder();
        $folder3->setName('Nouveau code - Wifi N°1');
        $folder3->setParent($folder2);
        $manager->persist($folder3);

        $folder4 = new Folder();
        $folder4->setName('TF1');
        $folder4->setParent($folder3);
        $manager->persist($folder4);

        $folder7 = new Folder();
        $folder7->setName('MEDIASQUARE');
        $folder7->setParent($folder2);
        $manager->persist($folder7);

        $folder5 = new Folder();
        $folder5->setName('Samsung');
        $folder5->setParent($folder7);
        $manager->persist($folder5);

        $folder6 = new Folder();
        $folder6->setName('Youtube');
        $folder6->setParent($folder3);
        $manager->persist($folder6);

        $folder8 = new Folder();
        $folder8->setName('Twitch');
        $folder8->setParent($folder3);
        $manager->persist($folder8);

        $manager->flush();

        // These references will be used inside video fixture
        $this->addReference('nouveau_code_wifi', $folder3);
        $this->addReference('tf1', $folder4);
        $this->addReference('samsung', $folder5);
        $this->addReference('youtube', $folder6);
    }
}