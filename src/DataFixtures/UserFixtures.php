<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        echo "load UserFixtures";
        $user1 = new User();
        $user1->setFirstname('Véran');
        $user1->setLastname('Desjardins');
        $user1->setEmail('veran@verandesjardins.dev');
        $manager->persist($user1);

        $user2 = new User();
        $user2->setFirstname('Thomas');
        $user2->setLastname('Lechat');
        $user2->setEmail('thomas@gmail.com');
        $manager->persist($user2);

        $manager->flush();

        // These references will be used inside video fixture
        $this->addReference('user1', $user1);
        $this->addReference('user2', $user2);
    }
}