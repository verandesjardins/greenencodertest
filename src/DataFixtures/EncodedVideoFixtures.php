<?php

namespace App\DataFixtures;

use App\Entity\EncodedVideo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EncodedVideoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $video1 = $this->getReference('video1');
        $video2 = $this->getReference('video2');
        $video3 = $this->getReference('video3');

        $encodedVideo1 = new EncodedVideo();
        $encodedVideo1->setVideo($video1);
        $encodedVideo1->setSize(123456);
        $encodedVideo1->setViews(1234);
        $manager->persist($encodedVideo1);

        $encodedVideo2 = new EncodedVideo();
        $encodedVideo2->setVideo($video2);
        $encodedVideo2->setSize(123456);
        $encodedVideo2->setViews(1234);
        $manager->persist($encodedVideo2);

        $encodedVideo3 = new EncodedVideo();
        $encodedVideo3->setVideo($video3);
        $encodedVideo3->setSize(123456);
        $encodedVideo3->setViews(1234);
        $manager->persist($encodedVideo3);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            VideoFixtures::class,
        ];
    }
}