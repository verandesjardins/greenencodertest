<?php

namespace App\DataFixtures;

use App\Entity\VideoQuality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class VideoQualityFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        echo "load videoquality";
        $videoQuality = new VideoQuality();
        $videoQuality->setLabel('1920x1080');
        $manager->persist($videoQuality);

        $manager->flush();

        // This reference will be used inside video fixture
        $this->addReference('video_quality', $videoQuality);
        echo "ref to video_quality added";
    }
}