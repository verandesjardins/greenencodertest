<?php

namespace App\Repository;

use App\Entity\VideoQuality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VideoQuality>
 *
 * @method VideoQuality|null find($id, $lockMode = null, $lockVersion = null)
 * @method VideoQuality|null findOneBy(array $criteria, array $orderBy = null)
 * @method VideoQuality[]    findAll()
 * @method VideoQuality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoQualityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VideoQuality::class);
    }

//    /**
//     * @return VideoQuality[] Returns an array of VideoQuality objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VideoQuality
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
