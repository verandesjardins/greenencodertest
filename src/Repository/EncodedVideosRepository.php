<?php

namespace App\Repository;

use App\Entity\EncodedVideos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EncodedVideos>
 *
 * @method EncodedVideos|null find($id, $lockMode = null, $lockVersion = null)
 * @method EncodedVideos|null findOneBy(array $criteria, array $orderBy = null)
 * @method EncodedVideos[]    findAll()
 * @method EncodedVideos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EncodedVideosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EncodedVideos::class);
    }

//    /**
//     * @return EncodedVideos[] Returns an array of EncodedVideos objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EncodedVideos
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
