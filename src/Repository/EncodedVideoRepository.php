<?php

namespace App\Repository;

use App\Entity\EncodedVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EncodedVideo>
 *
 * @method EncodedVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method EncodedVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method EncodedVideo[]    findAll()
 * @method EncodedVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EncodedVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EncodedVideo::class);
    }

//    /**
//     * @return EncodedVideo[] Returns an array of EncodedVideo objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?EncodedVideo
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
