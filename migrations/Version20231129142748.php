<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231129142748 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE encoded_videos (id INT AUTO_INCREMENT NOT NULL, video_id INT NOT NULL, views SMALLINT DEFAULT NULL, INDEX IDX_669C000429C1004E (video_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE folder (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_ECA209CD727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, quality_id INT NOT NULL, folder_id INT NOT NULL, name VARCHAR(255) NOT NULL, duration INT NOT NULL, size SMALLINT NOT NULL, INDEX IDX_7CC7DA2CBCFC6D57 (quality_id), INDEX IDX_7CC7DA2C162CB942 (folder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video_quality (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE encoded_videos ADD CONSTRAINT FK_669C000429C1004E FOREIGN KEY (video_id) REFERENCES video (id)');
        $this->addSql('ALTER TABLE folder ADD CONSTRAINT FK_ECA209CD727ACA70 FOREIGN KEY (parent_id) REFERENCES folder (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2CBCFC6D57 FOREIGN KEY (quality_id) REFERENCES video_quality (id)');
        $this->addSql('ALTER TABLE video ADD CONSTRAINT FK_7CC7DA2C162CB942 FOREIGN KEY (folder_id) REFERENCES folder (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE encoded_videos DROP FOREIGN KEY FK_669C000429C1004E');
        $this->addSql('ALTER TABLE folder DROP FOREIGN KEY FK_ECA209CD727ACA70');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2CBCFC6D57');
        $this->addSql('ALTER TABLE video DROP FOREIGN KEY FK_7CC7DA2C162CB942');
        $this->addSql('DROP TABLE encoded_videos');
        $this->addSql('DROP TABLE folder');
        $this->addSql('DROP TABLE video');
        $this->addSql('DROP TABLE video_quality');
    }
}
